package com.b.myapplication;

import android.annotation.SuppressLint;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

     ViewPager  mSlideViewPager;
     LinearLayout mDotLayout;
     SlideAdapter sliderAdapter;
     TextView [] mDots;


    ImageView lightMale;
    ImageView lightFemale;
    ImageView male;
    ImageView female;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Typeface comicSensFont = Typeface.createFromAsset(getAssets(),"comic_sens.ttf");
        final EditText name_edit_text = (EditText)findViewById(R.id.nickNameEditText);
        final EditText age_edit_text = (EditText)findViewById(R.id.AgeEditText);
        name_edit_text.setTypeface(comicSensFont);
        age_edit_text.setTypeface(comicSensFont);


        this.mSlideViewPager  = (ViewPager) findViewById(R.id.SlideViewPager);
        this.mDotLayout = (LinearLayout) findViewById(R.id.dotsLayout);

        sliderAdapter = new SlideAdapter(this);

        mSlideViewPager.setAdapter(sliderAdapter);
        addDotsInticator(0);
        mSlideViewPager.addOnPageChangeListener(viewListener);

        male = (ImageView) findViewById(R.id.MaleImageView);
        female = (ImageView) findViewById(R.id.FemaleImageView);
        lightMale = (ImageView) findViewById(R.id.MaleLightImageView);
        lightFemale = (ImageView) findViewById(R.id.FemaleLightImageView);

        lightMale.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onClick(View v) {
                if(male.getVisibility() == View.INVISIBLE)
                {
                    male.setVisibility(View.VISIBLE);
                    female.setVisibility(View.INVISIBLE);
                }
                else if(male.getVisibility() == View.VISIBLE)
                {
                    male.setVisibility(View.INVISIBLE);
                }
                name_edit_text.setTextColor(getResources().getColor(R.color.blue));
                age_edit_text.setTextColor(getResources().getColor(R.color.blue));
                name_edit_text.setHintTextColor(getResources().getColor(R.color.blue));
                age_edit_text.setHintTextColor(getResources().getColor(R.color.blue));

            }
        });

        lightFemale.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onClick(View v) {
                if(female.getVisibility() == View.INVISIBLE)
                {
                    female.setVisibility(View.VISIBLE);
                    male.setVisibility(View.INVISIBLE);
                }
                else if(female.getVisibility() == View.VISIBLE)
                {
                    female.setVisibility(View.INVISIBLE);
                }
                name_edit_text.setTextColor(getResources().getColor(R.color.pink));
                age_edit_text.setTextColor(getResources().getColor(R.color.pink));
                name_edit_text.setHintTextColor(getResources().getColor(R.color.pink));
                age_edit_text.setHintTextColor(getResources().getColor(R.color.pink));
            }
        });






    }

    @SuppressLint("RtlHardcoded")
    public void addDotsInticator(int position){
        this.mDots = new TextView[5];
        mDotLayout.removeAllViews();
        for(int i= 0;i< this.mDots.length;i++){
            mDots[i] = new TextView(this);
            mDots[i].setText(Html.fromHtml("&#8226;"));
            mDots[i].setTextSize(40);
            mDots[i].setTextColor(getResources().getColor(R.color.TransperdBlack));
            //mDots[i].setGravity(LEFT);

            mDotLayout.addView(mDots[i]);
        }

        if(mDots.length>0){
            mDots[position].setTextColor(getResources().getColor(R.color.black));
        }
    }

        ViewPager.OnPageChangeListener viewListener = new ViewPager.OnPageChangeListener(){

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {


            }

            @Override
            public void onPageSelected(int position) {
                addDotsInticator(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        };



}
